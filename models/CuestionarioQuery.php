<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Cuestionario]].
 *
 * @see Cuestionario
 */
class CuestionarioQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Cuestionario[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Cuestionario|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
