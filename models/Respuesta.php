<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "respuesta".
 *
 * @property int $id
 * @property int $id_cuestionario
 * @property int $id_pregunta
 * @property string $nick_usuario
 * @property string $respuesta
 *
 * @property Cuestionario $cuestionario
 * @property Pregunta $pregunta
 */
class Respuesta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'respuesta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cuestionario', 'id_pregunta'], 'required'],
            [['id_cuestionario', 'id_pregunta'], 'integer'],
            [['nick_usuario'], 'string', 'max' => 255],
            [['respuesta'], 'string', 'max' => 40],
            [['id_cuestionario'], 'unique'],
            [['id_cuestionario'], 'exist', 'skipOnError' => true, 'targetClass' => Cuestionario::className(), 'targetAttribute' => ['id_cuestionario' => 'id']],
            [['id_pregunta'], 'exist', 'skipOnError' => true, 'targetClass' => Pregunta::className(), 'targetAttribute' => ['id_pregunta' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cuestionario' => 'Id Cuestionario',
            'id_pregunta' => 'Id Pregunta',
            'nick_usuario' => 'Nick Usuario',
            'respuesta' => 'Respuesta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuestionario()
    {
        return $this->hasOne(Cuestionario::className(), ['id' => 'id_cuestionario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPregunta()
    {
        return $this->hasOne(Pregunta::className(), ['id' => 'id_pregunta']);
    }

    /**
     * {@inheritdoc}
     * @return RespuestaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RespuestaQuery(get_called_class());
    }

    public static function saveRecord($id_cuestionario, $id_pregunta, $nick,$dato){
        $respuesta = new Respuesta;
       $respuesta->id_cuestionario = $id_cuestionario;
       $respuesta->id_pregunta = $id_pregunta;
       $respuesta->nick_usuario = $nick;
       $respuesta->respuesta = $dato;
       $respuesta->save();
    }
}
