<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pregunta;

/**
 * PreguntaSearch represents the model behind the search form of `app\models\Pregunta`.
 */
class PreguntaSearch extends Pregunta
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_cuestionario'], 'integer'],
            [['pregunta', 'respuesta_1', 'respuesta_2', 'respuesta_3', 'respuesta_4', 'respuesta_5', 'respuesta_correcta'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pregunta::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_cuestionario' => $this->id_cuestionario,
        ]);

        $query->andFilterWhere(['like', 'pregunta', $this->pregunta])
            ->andFilterWhere(['like', 'respuesta_1', $this->respuesta_1])
            ->andFilterWhere(['like', 'respuesta_2', $this->respuesta_2])
            ->andFilterWhere(['like', 'respuesta_3', $this->respuesta_3])
            ->andFilterWhere(['like', 'respuesta_4', $this->respuesta_4])
            ->andFilterWhere(['like', 'respuesta_5', $this->respuesta_5])
            ->andFilterWhere(['like', 'respuesta_correcta', $this->respuesta_correcta]);

        return $dataProvider;
    }
}
