<?php

namespace app\controllers;

use Yii;
use app\models\Cuestionario;
use app\models\CuestionarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CuestionarioController implements the CRUD actions for Cuestionario model.
 */
class IdentificacionController extends Controller
{
	public function actionIndex()
    {
        return $this->render('index');
    }
}