<?php

namespace app\controllers;

use Yii;
use app\models\Cuestionario;
use app\models\CuestionarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class PanelController extends Controller
{
	public function actionIndex()
    {

    	$max = Cuestionario::find()->select("id")->max('id');
    	return $this->redirect(['/cuestionario/view', 'id' => $max]);

    }
}